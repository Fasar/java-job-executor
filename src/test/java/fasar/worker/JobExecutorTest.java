package fasar.worker;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.*;
import java.util.stream.Collectors;

public class JobExecutorTest {

    public static void main(String[] args) throws InterruptedException {
        ScheduledExecutorService executor = Executors.newScheduledThreadPool(8);
        JobExecutor jobExecutor = new JobExecutor(5L*60*1000, executor );
        List<CompletableFuture<Void>> futures = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            String jobName = String.format("procdbjob-%03d", i);
            CompletableFuture<Void> task = jobExecutor.scheduleJob("task1", jobName, (run) -> {
                if ("procdbjob-013".equals(jobName)) {
                    throw new RuntimeException("Not good");
                }
                int counter = 0;
                while (counter < 30 && run.get()) {
                    //System.out.println(jobName + " - loop " + counter);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    counter++;
                }
                if (counter == 30) {
                    System.out.println(jobName + " - Finish");
                } else {
                    System.out.println(jobName + " - Abort");
                }
                return Optional.empty();

            });
            futures.add(task);
        }

        // printQueue("1", jobExecutor);
        Thread.sleep(3000);
        futures.get(50).cancel(true);

        Thread.sleep(3000);
        // printQueue("2", jobExecutor);

        for (CompletableFuture<Void> future : futures) {
            future.cancel(true);
        }

        printQueue("3", jobExecutor);

        Thread.sleep(3000);
        printQueue("4", jobExecutor);


        Thread.sleep(3000);
        printQueue("4", jobExecutor);

        executor.shutdownNow();
        executor.awaitTermination(1, TimeUnit.SECONDS);
        System.out.println("End of main");
    }

    private static void printQueue(String s, JobExecutor jobExecutor) {
        ConcurrentLinkedQueue<JobDescription<?>> queue = jobExecutor.listJobs();
        System.out.println(s);
        String collect = queue.stream()
            .map(JobDescription::toString)
            .collect(Collectors.joining(",\n   ", "   ", ""));
        System.out.println(collect);
    }
}

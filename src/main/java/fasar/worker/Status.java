package fasar.worker;

public enum Status {
    PENDING, RUNNING, ABORT, FINISHED, ERROR
}

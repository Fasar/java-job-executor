package fasar.worker;

import java.time.Instant;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * This class wrap a job with some fields used as meta data.
 *
 * It is usefull to get the information on a job.
 * If it is complete, aborded or the time it takes to finish.
 *
 * @param <T>
 */
public class JobDescription<T> {

    private final AtomicBoolean okToRun = new AtomicBoolean(true);
    private String task;
    private String name;
    private Status status;
    private Instant scheduleTime;
    private Instant startTime;
    private Instant endTime;
    private Exception exception = null;
    private Job<T> job;

    public JobDescription(
        String task, String name, Status status,
        Instant scheduleTime, Instant startTime,
        Instant endTime,
        Job<T> job
    ) {
        this.task = task;
        this.name = name;
        this.status = status;
        this.scheduleTime = scheduleTime;
        this.startTime = startTime;
        this.endTime = endTime;
        this.job = job;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Instant getScheduleTime() {
        return scheduleTime;
    }

    public void setScheduleTime(Instant scheduleTime) {
        this.scheduleTime = scheduleTime;
    }

    public Instant getStartTime() {
        return startTime;
    }

    public void setStartTime(Instant startTime) {
        this.startTime = startTime;
    }

    public Instant getEndTime() {
        return endTime;
    }

    public void setEndTime(Instant endTime) {
        this.endTime = endTime;
    }

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }

    public Job<T> getJob() {
        return job;
    }

    public void setJob(Job<T> job) {
        this.job = job;
    }

    public AtomicBoolean getOkToRun() {
        return okToRun;
    }

    @Override
    public String toString() {
        return "JobDescription{" +
            "task='" + task + '\'' +
            ", name='" + name + '\'' +
            ", status=" + status +
            ", scheduleTime=" + scheduleTime +
            ", startTime=" + startTime +
            ", endTime=" + endTime +
            '}';
    }
}

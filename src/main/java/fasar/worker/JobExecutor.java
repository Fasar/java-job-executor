package fasar.worker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Named;
import java.time.Instant;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;


/**
 * A simple job executor to create task that are cancelable, logged in a list  and with some meta-data.
 *
 */
@Named
public class JobExecutor {
    private static Logger LOG = LoggerFactory.getLogger(JobExecutor.class);

    private ConcurrentLinkedQueue<JobDescription<?>> queue = new ConcurrentLinkedQueue<>();
    private ConcurrentLinkedQueue<MyCompletableFuture<?>> tasks = new ConcurrentLinkedQueue<>();
    private long maxAge;
    private ScheduledExecutorService defaultExecutor;

    @Inject
    public JobExecutor(
        long maxAge,
        ScheduledExecutorService defaultExecutor
    ) {
        this.maxAge = maxAge;
        this.defaultExecutor = defaultExecutor;
    }

    public <T> MyCompletableFuture<T> scheduleJob(
        String task, String name,
        Job<T> job
    ) {
        return scheduleJob(task, name, job, defaultExecutor);
    }

    public <T> MyCompletableFuture<T> scheduleJob(
        String task, String name,
        Job<T> job,
        ScheduledExecutorService executor
    ) {
        JobDescription<T> jobDescription = new JobDescription(
            task, name, Status.PENDING,
            Instant.now(), null, null,
            job
        );
        queue.add(jobDescription);
        CompletableFuture<T> future = CompletableFuture.supplyAsync(
            () -> {
                T elem = doJob(jobDescription);
                // Remove the task in 5 minutes
                executor.schedule(() -> {
                    queue.remove(jobDescription);
                }, maxAge, TimeUnit.MILLISECONDS);
                tasks.removeIf(e -> e.getWrapped() == jobDescription);
                return elem;
            },
            executor
        );

        MyCompletableFuture res = new MyCompletableFuture(jobDescription, future);
        tasks.add(res);
        return res;
    }

    private <T> T doJob(JobDescription<T> jobDescription) {
        jobDescription.setStatus(Status.RUNNING);
        jobDescription.setStartTime(Instant.now());
        Optional<T> run = Optional.empty();
        AtomicBoolean okToRun = jobDescription.getOkToRun();
        try {
            Job<T> job = jobDescription.getJob();
            run = job.run(okToRun);

            if ((run == null || !run.isPresent()) && !okToRun.get()) {
                jobDescription.setStatus(Status.ABORT);
            } else {
                jobDescription.setStatus(Status.FINISHED);
            }
        } catch (Exception e) {
            jobDescription.setStatus(Status.ERROR);
            jobDescription.setException(e);
            LOG.error("Cant't exec the job {}.{} : {}", jobDescription.getTask(), jobDescription.getName(), e.getMessage(), e);
            throw new RuntimeException(e);
        } finally {
            jobDescription.setEndTime(Instant.now());
        }

        return run == null ? null : run.orElse(null);
    }


    public ConcurrentLinkedQueue<JobDescription<?>> listJobs() {
        return queue;
    }

    public ConcurrentLinkedQueue<MyCompletableFuture<?>> listTasks() {
        return tasks;
    }
}

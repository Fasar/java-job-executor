package fasar.worker;

import java.time.Instant;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * A class to implement the CompletableFuture.
 *
 * It describes the Future (Promise) of a Job.
 *
 * A job can be canceled with the cancel.
 * It will false the parameter's boolean okToRun given to the job as parameter.
 *
 * It will update the meta data JobDescription of the job
 *
 * @param <T>
 */
public class MyCompletableFuture<T> extends CompletableFuture<T> {

    private CompletableFuture<T> future;
    private JobDescription jobDescription;

    public MyCompletableFuture(JobDescription jobDescription, CompletableFuture<T> future) {
        this.jobDescription = jobDescription;
        this.future = future;
    }

    public JobDescription getWrapped() {
        return jobDescription;
    }

    @Override
    public boolean cancel(boolean mayInterruptIfRunning) {
        boolean cancel = future.cancel(mayInterruptIfRunning);
        AtomicBoolean continueToRun = jobDescription.getOkToRun();
        continueToRun.set(false);
        if (jobDescription.getStatus() == Status.PENDING) {
            jobDescription.setStatus(Status.ABORT);
            jobDescription.setEndTime(Instant.now());
        }
        return cancel;
    }


    public boolean isDone() {
        return future.isDone();
    }

    public T get() throws InterruptedException, ExecutionException {
        return future.get();
    }

    public T get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        return future.get(timeout, unit);
    }

    public T join() {
        return future.join();
    }

    public T getNow(T valueIfAbsent) {
        return future.getNow(valueIfAbsent);
    }

    public boolean complete(T value) {
        return future.complete(value);
    }

    public boolean completeExceptionally(Throwable ex) {
        return future.completeExceptionally(ex);
    }

    public <U> CompletableFuture<U> thenApply(Function<? super T, ? extends U> fn) {
        return future.thenApply(fn);
    }

    public <U> CompletableFuture<U> thenApplyAsync(Function<? super T, ? extends U> fn) {
        return future.thenApplyAsync(fn);
    }

    public <U> CompletableFuture<U> thenApplyAsync(Function<? super T, ? extends U> fn, Executor executor) {
        return future.thenApplyAsync(fn, executor);
    }

    public CompletableFuture<Void> thenAccept(Consumer<? super T> action) {
        return future.thenAccept(action);
    }

    public CompletableFuture<Void> thenAcceptAsync(Consumer<? super T> action) {
        return future.thenAcceptAsync(action);
    }

    public CompletableFuture<Void> thenAcceptAsync(Consumer<? super T> action, Executor executor) {
        return future.thenAcceptAsync(action, executor);
    }

    public CompletableFuture<Void> thenRun(Runnable action) {
        return future.thenRun(action);
    }

    public CompletableFuture<Void> thenRunAsync(Runnable action) {
        return future.thenRunAsync(action);
    }

    public CompletableFuture<Void> thenRunAsync(Runnable action, Executor executor) {
        return future.thenRunAsync(action, executor);
    }

    public <U, V> CompletableFuture<V> thenCombine(CompletionStage<? extends U> other, BiFunction<? super T, ? super U, ? extends V> fn) {
        return future.thenCombine(other, fn);
    }

    public <U, V> CompletableFuture<V> thenCombineAsync(CompletionStage<? extends U> other, BiFunction<? super T, ? super U, ? extends V> fn) {
        return future.thenCombineAsync(other, fn);
    }

    public <U, V> CompletableFuture<V> thenCombineAsync(CompletionStage<? extends U> other, BiFunction<? super T, ? super U, ? extends V> fn, Executor executor) {
        return future.thenCombineAsync(other, fn, executor);
    }

    public <U> CompletableFuture<Void> thenAcceptBoth(CompletionStage<? extends U> other, BiConsumer<? super T, ? super U> action) {
        return future.thenAcceptBoth(other, action);
    }

    public <U> CompletableFuture<Void> thenAcceptBothAsync(CompletionStage<? extends U> other, BiConsumer<? super T, ? super U> action) {
        return future.thenAcceptBothAsync(other, action);
    }

    public <U> CompletableFuture<Void> thenAcceptBothAsync(CompletionStage<? extends U> other, BiConsumer<? super T, ? super U> action, Executor executor) {
        return future.thenAcceptBothAsync(other, action, executor);
    }

    public CompletableFuture<Void> runAfterBoth(CompletionStage<?> other, Runnable action) {
        return future.runAfterBoth(other, action);
    }

    public CompletableFuture<Void> runAfterBothAsync(CompletionStage<?> other, Runnable action) {
        return future.runAfterBothAsync(other, action);
    }

    public CompletableFuture<Void> runAfterBothAsync(CompletionStage<?> other, Runnable action, Executor executor) {
        return future.runAfterBothAsync(other, action, executor);
    }

    public <U> CompletableFuture<U> applyToEither(CompletionStage<? extends T> other, Function<? super T, U> fn) {
        return future.applyToEither(other, fn);
    }

    public <U> CompletableFuture<U> applyToEitherAsync(CompletionStage<? extends T> other, Function<? super T, U> fn) {
        return future.applyToEitherAsync(other, fn);
    }

    public <U> CompletableFuture<U> applyToEitherAsync(CompletionStage<? extends T> other, Function<? super T, U> fn, Executor executor) {
        return future.applyToEitherAsync(other, fn, executor);
    }

    public CompletableFuture<Void> acceptEither(CompletionStage<? extends T> other, Consumer<? super T> action) {
        return future.acceptEither(other, action);
    }

    public CompletableFuture<Void> acceptEitherAsync(CompletionStage<? extends T> other, Consumer<? super T> action) {
        return future.acceptEitherAsync(other, action);
    }

    public CompletableFuture<Void> acceptEitherAsync(CompletionStage<? extends T> other, Consumer<? super T> action, Executor executor) {
        return future.acceptEitherAsync(other, action, executor);
    }

    public CompletableFuture<Void> runAfterEither(CompletionStage<?> other, Runnable action) {
        return future.runAfterEither(other, action);
    }

    public CompletableFuture<Void> runAfterEitherAsync(CompletionStage<?> other, Runnable action) {
        return future.runAfterEitherAsync(other, action);
    }

    public CompletableFuture<Void> runAfterEitherAsync(CompletionStage<?> other, Runnable action, Executor executor) {
        return future.runAfterEitherAsync(other, action, executor);
    }

    public <U> CompletableFuture<U> thenCompose(Function<? super T, ? extends CompletionStage<U>> fn) {
        return future.thenCompose(fn);
    }

    public <U> CompletableFuture<U> thenComposeAsync(Function<? super T, ? extends CompletionStage<U>> fn) {
        return future.thenComposeAsync(fn);
    }

    public <U> CompletableFuture<U> thenComposeAsync(Function<? super T, ? extends CompletionStage<U>> fn, Executor executor) {
        return future.thenComposeAsync(fn, executor);
    }

    public CompletableFuture<T> whenComplete(BiConsumer<? super T, ? super Throwable> action) {
        return future.whenComplete(action);
    }

    public CompletableFuture<T> whenCompleteAsync(BiConsumer<? super T, ? super Throwable> action) {
        return future.whenCompleteAsync(action);
    }

    public CompletableFuture<T> whenCompleteAsync(BiConsumer<? super T, ? super Throwable> action, Executor executor) {
        return future.whenCompleteAsync(action, executor);
    }

    public <U> CompletableFuture<U> handle(BiFunction<? super T, Throwable, ? extends U> fn) {
        return future.handle(fn);
    }

    public <U> CompletableFuture<U> handleAsync(BiFunction<? super T, Throwable, ? extends U> fn) {
        return future.handleAsync(fn);
    }

    public <U> CompletableFuture<U> handleAsync(BiFunction<? super T, Throwable, ? extends U> fn, Executor executor) {
        return future.handleAsync(fn, executor);
    }

    public CompletableFuture<T> toCompletableFuture() {
        return future.toCompletableFuture();
    }

    public CompletableFuture<T> exceptionally(Function<Throwable, ? extends T> fn) {
        return future.exceptionally(fn);
    }


    public boolean isCancelled() {
        return future.isCancelled();
    }

    public boolean isCompletedExceptionally() {
        return future.isCompletedExceptionally();
    }

    public void obtrudeValue(T value) {
        future.obtrudeValue(value);
    }

    public void obtrudeException(Throwable ex) {
        future.obtrudeException(ex);
    }

    public int getNumberOfDependents() {
        return future.getNumberOfDependents();
    }
}

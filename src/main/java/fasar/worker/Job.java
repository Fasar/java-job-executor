package fasar.worker;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;


/**
 * This is a description of a Job / a task / some algorithm to run with the
 * JobExecutor.
 *
 * @param <T> The class to return. &lt;Void&gt; if not needed.
 */
public interface Job<T> {

    /**
     *
     * The Job to run should take care of okToRun.
     *
     * if OkToRun is false, the procdbjob should stop the Job and return.
     *
     * The return can be the value or Optional.empty if the Job is stopped
     * with the okToRun
     *
     * @param okToRun
     * @return
     */
    Optional<T> run(AtomicBoolean okToRun) throws Exception;

}
